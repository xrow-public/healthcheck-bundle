<?php

declare(strict_types=1);

namespace Xrow\Bundle\HealthCheck\Controller;

use Doctrine\DBAL\Connection;
use Exception;
use Ibexa\Bundle\Core\Controller;
use League\Flysystem\Filesystem as StorageFilesystem;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class HealthCheckController extends Controller
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var StorageFilesystem
     */
    private $filesystem;

    /**
     * @var Filesystem
     */
    private $localFilesystem;

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var TagAwareAdapterInterface
     */
    protected $pool;

    /**
     * @param LoggerInterface $logger
     * @param Connection $connection
     * @param FilesystemInterface $filesystem
     * @param TagAwareCacheInterface $cache
     */
    public function __construct(LoggerInterface $logger, Connection $connection, StorageFilesystem $filesystem, TagAwareCacheInterface $cache)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->filesystem = $filesystem;
        $this->cache = $cache;
        $this->localFilesystem = new Filesystem();
    }

    /**
     * @Route("/check", methods={"GET"}, name = "check")
     */
    public function healthCheckAction()
    {
        try {
            $this->checkDatabase();
        } catch (Exception $exception) {
            $this->logger->critical('Distributed Storage fail', ['exception' => $exception]);

            return new Response('Database fail', Response::HTTP_SERVICE_UNAVAILABLE, [
                'Content-Type' => 'text/plain',
            ]);
        }

        try {
            $this->checkCache();
        } catch (Exception $exception) {
            $this->logger->critical('Cache fail', ['exception' => $exception]);

            return new Response('Cache fail', Response::HTTP_SERVICE_UNAVAILABLE, [
                'Content-Type' => 'text/plain',
            ]);
        }

        try {
            $this->checkStorage();
        } catch (Exception $exception) {
            $this->logger->critical('Distributed Storage fail', ['exception' => $exception]);

            return new Response('Distributed Storage fail', Response::HTTP_SERVICE_UNAVAILABLE, [
                'Content-Type' => 'text/plain',
            ]);
        }
        try {
            $this->checkLocalDisk();
        } catch (Exception $exception) {
            $this->logger->critical('Local Storage fail', ['exception' => $exception]);

            return new Response('Local Storage fail', Response::HTTP_SERVICE_UNAVAILABLE, [
                'Content-Type' => 'text/plain',
            ]);
        }

        return new Response('OK', Response::HTTP_OK, [
            'Content-Type' => 'text/plain',
        ]);
    }

    /**
     * Checks if local disk is writeable.
     *
     * return true if local disk is writeable
     */
    private function checkLocalDisk()
    {
        $filename = $this->container->getParameter('kernel.cache_dir') . '/health-check.tmp';
        $this->localFilesystem->dumpFile($filename, 'health-check file');
        $this->localFilesystem->remove($filename);
        try {
            $this->localFilesystem->dumpFile($filename, 'health-check file');
            $this->localFilesystem->remove($filename);
        } catch (Exception $exception) {
            $this->localFilesystem->remove($filename);
            throw $exception;
        }

        return true;
    }

    /**
     * Checks if storage is writeable
     * Is using League\Flysystem\Filesystem.
     *
     * return true if storage is writeable
     */
    private function checkStorage()
    {
        $filename = './health-check.tmp';
        try {
            $this->filesystem->write($filename, 'health-check file');
            $this->filesystem->delete($filename);
        } catch (Exception $exception) {
            $this->filesystem->delete($filename);
            throw $exception;
        }

        return true;
    }

    /**
     * Checks if cache is writeable and readable.
     *
     * return true if cache is writeable and readable
     */
    private function checkCache()
    {
        try {
            $cacheItem = $this->cache->getItem('health-check-item');
            $cacheItem->isHit();
            $cacheItem->set('1');
            $cacheItem->tag(['health-check-tag']);
            $this->cache->save($cacheItem);
        } catch (Exception $exception) {
            throw $exception;
        }

        return true;
    }

    /**
     * Checks if database is available
     * Is using doctrine.
     *
     * return true if database is available
     */
    private function checkDatabase()
    {
        $sql = 'SELECT * FROM ezcontentobject limit 0,1';
        $stmt = $this->connection->prepare($sql);
        try {
            $stmt->execute();
        } catch (Exception $exception) {
            $this->logger->critical('Database fail', ['exception' => $exception]);
            throw $exception;
        }

        return true;
    }
}
