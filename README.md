# Health check Bundle

Health check features for Ibexa websites.

## Install

```bash
composer config --json extra.symfony.endpoint \
'["https://api.github.com/repos/xrowgmbh/recipes/contents/index.json?ref=main","https://api.github.com/repos/ibexa/recipes/contents/index.json?ref=flex/main","flex://defaults"]' 
```

```bash
composer require xrow/healthcheck-bundle
```

## Use

```bash
curl http://localhost:8080/check
```

## Contribute

```bash
composer req xrow/healthcheck-bundle:dev-master --prefer-source
```

Create merge request.